<?php

namespace App\Tests\Func;

use PHPUnit\Framework\TestCase;
use App\Services\HomeServices;
class HomeServicesFuncTest extends TestCase
{
    public function testAdding(): void
    {
        $services = new HomeServices();
        $this->assertEquals($services::add(4,2),6);
    }
}
