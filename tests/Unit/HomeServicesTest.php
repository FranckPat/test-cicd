<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\HomeServices;
class HomeServicesTest extends TestCase
{
    public function testAdding(): void
    {
        $services = new HomeServices();
        $this->assertEquals($services::add(4,2),6);
    }
}
